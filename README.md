# Repository für TuSLi-Dokumente

Hier werden wichtige Dokumente des Vereins unter Versionsverwaltung gestellt.

Dazu zählen unter anderem:
 * die Satzung
 * Ordnungen
   * Finanzordnung
   * Beitragsordnung
   * Wahlordnung

