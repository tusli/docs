# Beitragsordnung

(gemäß § 10 der Vereinssatzung)

1. Diese Beitragsordnung regelt alle Einzelheiten über die Pflichten der Mitglieder zur Entrichtung von Beiträgen und Gebühren an den Verein. Sie ist Bestandteil des Aufnahmeantrags.

2. Der Jahresbeitrag setzt sich aus einem Jahresgrundbeitrag (JGB), den jeweiligen Abteilungsbeiträgen (ASBs) und ggf. Umlagen zusammen.

3. Es bestehen nachstehende Zahlungsverpflichtungen:
   - Jahresgrundbeiträge (JGBs): Sie bestehen aus dem jeweiligen Grundbeitrag für Kinder und Jugendliche unter 18 Jahren, für Erwachsene über 18 Jahre und Passive. Sie werden vom Vereinstag beschlossen (§ 16 Satz 10 der Satzung).
   - Vereinsumlagen: Sie werden vom Vereinstag beschlossen (§ 16 Satz 10 der Satzung)und können nach Mitgliedergruppen differenziert werden.
   - Einmalige Aufnahmegebühr: Sie kann für Kinder und Jugendliche unter 18 Jahren und für Erwachsene über 18 Jahre differenziert werden und wird vom Vereinstag beschlossen (§16 Satz 10 der Satzung).
   - Erhöhungsbeitrag (§10 Satz 4 der Satzung): Er beträgt bei Zahlungsverzug im 1. Kalenderhalbjahr EUR 20,00 und danach in jedem folgenden Kalenderhalbjahr zusätzlich EUR 20,00.
   - Abteilungsbeiträge (ASBs): Sie bestehen aus dem ASB1 = Sportbeitrag und dem ASB2 = Trainerbeitrag, können nach Gruppen von Abteilungsangehörigen differenzieren und werden von den Abteilungsversammlungen für ihre Zugehörigen beschlossen (§10 Satz 2, §22 Satz 6 der Satzung)
   - Abteilungsumlagen: Sie werden für ihren Fachbereich von den Abteilungsversammlungen beschlossen.
   - Bei Sondermitgliedschaften oder außerhalb von Abteilungen betreuten Mitgliedern nach § 8 Satz 13 der Satzung setzt der Vorstand mit Zustimmung des Vereinsrates kostendeckende ASBs fest.
   - Jahresgrundbeitrag und die Abteilungsbeiträge ASB1 und ASB2 bilden unter Berücksichtigung der Vorgaben des LSB den Jahresbeitrag (§10 Satz 2, §22 Satz 6 der Satzung).
   - Vereins- oder Abteilungsumlagen sind nur bei außerordentlichem Finanzbedarf unter Angabe des Verwendungszwecks zulässig und neben dem Jahresbeitrag zu entrichten.
   - Bearbeitungsgebühren für Sonderleistungen bei fehlgeschlagenem Lastschrifteinzug betragen EUR 5,00 je Fall.

     Weitere Gebühren kann der Vorstand festlegen.

4. Jahresgrundbeiträge:
   - Kinder und Jugendliche bis 17 Jahren
   - Erwachsene über 18 Jahre
   - Ehrenmitglieder und Mitglieder mit langjähriger Mitgliedschaft (§10 Satz 8 der Satzung)
   - Passive

   Die Aufnahmegebühr in den Verein beträgt für
   - Kinder und Jugendliche bis 17 Jahren
   - Erwachsene über 18 Jahre

5. Die gesonderte Beitragstabelle ist Bestandteil der Beitragsordnung und beinhaltet die Höhe der
   - Aufnahmegebühren
   - Jahresgrundbeiträge
   - Abteilungsbeiträge
   - evtl. Umlagen
   
6. Bei Vereinseintritt im Zeitraum
   - 01\. Januar bis 31. März ist der volle Jahresbeitrag
   - 01\. April bis 30. Juni sind 3/4 des Jahresbeitrags
   - 01\. Juli bis 30. September ist der halbe Jahresbeitrag
   - Oktober bis 31. Dezember sind 1/4 des Jahresbeitrags zu entrichten.
   - Ausnahmen sind der Beitragstabelle zu entnehmen.
   
7. Für die Beitragshöhe ist der am Fälligkeitstag bestehende Mitgliederstatus maßgebend.

8. Alle Beiträge sind als Bringschuld grundsätzlich im Voraus und bargeldlos in EURO zu entrichten.

9. Alle Beiträge werden vom Verein erhoben und verwaltet. Die ASB1 und evtl. Umlagen werden - unter Maßgabe der Finanzordnung - an die Abteilungen weitergeleitet.

10. Der gesamte Jahresbeitrag und etwaige Umlagen sind jeweils am 01. Januar fällig und bis spätestens 20. Januar zahlbar.

11. Die Zahlungen an den Verein können per Lastschrifteinzug oder Überweisung erfolgen.

12. Unabhängig von der Zahlart wird nur **eine** Rechnung versandt, entweder als Jahresbeitrags- oder als Neueintrittsrechnung.
13. Bei Erteilung einer Lastschriftermächtigung ist auch eine 1/4- oder 1/2-jährliche Zahlung in gleichen Raten möglich.
14. Zahlungstermine:
    - bei Neueintritt: Sofort
    - bei jährlicher Zahlung: Der 01.01.
    - bei Halbjahres-Zahlung: Der 01.01. und 01.07.
    - bei Quartalszahlung und Sondermitgliedschaft „Spiel-Sport-Spaß“: Der 01.01., 01.04., 01.07.

    und 01.10.

    - bei monatlicher Zahlung und Sondermitgliedschaft „Kita-Turnen“: Immer der 1. des Monats
15. Abbuchungen sind nur von einem Girokonto möglich.
16. Mitglieder, die nicht am Abbuchungsverfahren teilnehmen, entrichten ihre Beiträge nur per Überweisung bis spätestens 20. Januar jeden Jahres auf das Beitragskonto des Vereins.

    Beitragskonto:

    |       |                        |           |              |
    |-------|------------------------|-----------|--------------|
    | Bank: | Deutsche Bank          |           |              |
    | IBAN: | DE61100700240884226200 | Kto.-Nr.: | 88 422 62 00 |
    | BIC:  | DEUTDEDBBER            | BLZ:      | 100 700 24   |


    Verwendungszweck: "Mitgliedsnummer lt. Rechnung"
17. Durch fehlende Kontodeckung oder -wechsel entstandene Lastschriftrückrechnungs- und Bearbeitungsgebühren seitens der Bank gehen, wie auch die hierfür vom Verein erhobene eigene Gebühr in Höhe von EUR 5,00, zu Lasten des Mitglieds.
18. Mitglieder, die ihrer Zahlungsverpflichtung nicht nachgekommen sind, werden mit einer Zahlungsfrist erinnert.
19. Im Falle des weiteren Zahlungssäumnis, nach erfolgloser Zahlungserinnerung, erhöht sich der Beitrag um EUR 20,00 pro angefangenem und jedem weiteren Halbjahr und wird durch Mahnung fällig gestellt.

    Bei andauerndem Zahlungsverzug im 2. Halbjahr ist der Verein nach § 286 BGB berechtigt, nach Ankündigung die Zahlungsforderungen an ein Inkassounternehmen zu übergeben.
20. Bei einem Vereinsaustritt bleibt die Forderung der bei Beendigung der Mitgliedschaft bis zu diesem Zeitpunkt fälligen gewordenen Beiträge bestehen (§12 Satz 6 der Satzung).
21. Eine Mitgliedschaft ohne Abteilungszugehörigkeit ist nicht möglich.
22. Abteilungswechsel und somit Änderungen der Abteilungsbeiträge sind nur in Absprache mit den beteiligten Abteilungen und zum Quartalsbeginn möglich.
23. Bei Nutzung mehrerer Abteilungen ist das Verlassen einer Abteilung nur in Absprache mit der betroffenen Abteilung und zum Quartalsbeginn möglich.
24. Der Übergang in eine zusätzliche Abteilung ist jederzeit möglich.
25. In allen drei Fällen findet ein Differenzausgleich der Abteilungsbeiträge statt.
26. Der Wechsel von einer aktiven zu einer passiven Mitgliedschaft ist nur zum Jahreswechsel möglich.
27. Der Wechsel von einer passiven zu einer aktiven Mitgliedschaft ist jederzeit möglich, es gilt Nr. 25 sinngemäß auch für den Jahresgrundbeitrag.
28. Für zusätzliche Sportangebote (Sportkurse, Rehabilitationsprogramme usw.) gelten gesonderte Gebühren, die im Einzelnen festgelegt werden.
29. Die für die Verwaltung notwendigen Daten werden elektronisch erfasst und gespeichert. Die personenbezogenen Daten der Mitglieder werden gemäß den geltenden datenschutzrechtlichen Vorschriften und Bestimmungen erhoben und verwendet.
30. Die Beitragsordnung wurde am 13.11.2014 vom Vereinstag beschlossen und steht auch auf der Homepage des Vereins (www.tusli.de) zur Einsicht zur Verfügung.
