# Finanzordnung

Stand: 23.01.2018

## Präambel

1. Der TuS Lichterfelde von 1887 (Berlin) e.V. – nachfolgend Verein genannt – gibt sich nach § 24 Ziffer 3 seiner Satzung die folgende Finanzordnung.

## § 1 Grundsätze

1. Der Verein ist nach den Grundsätzen der Wirtschaftlichkeit zu führen. Die Aufwendungen müssen in einem wirtschaftlichen Verhältnis zu den erzielten und erwarteten Erträgen stehen.

2. Für den Verein und für jede Abteilung gilt grundsätzlich das Kostendeckungsprinzip im Rahmen des Haushaltsplanes.

3. Der Vereinsrat kann auf Grundlage des Solidaritätsprinzips den Abteilungen zusätzliche Mittel zur Aufrechterhaltung des Sportbetriebes zur Verfügung stellen.

4. Die Mittel des Vereins dürfen nur für die satzungsgemäßen Zwecke verwendet werden.

5. Es darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

6. Der Finanzausschuss prüft die zweckgerechte Verwendung der Mittel des Vereins und der Abteilungen, die bestimmungsgemäße Verwendung von Zuwendungen sowie die Vermögenswerte. Hierzu sind die Mitglieder des Ausschusses berechtigt, regelmäßig und unangemeldet Prüfungen durchzuführen. Über das Ergebnis derartiger Prüfungen ist ein Vermerk anzufertigen. Der Vereinsrat ist durch Übersendung von Ergebnisprotokollen zu unterrichten.

## § 2 Haushaltsplan

1. Für jedes Geschäftsjahr muss vom Vorstand für den Geschäftsstellenbetrieb und von den Abteilungsleitungen ein Haushaltsplan aufgestellt werden. Um die satzungsgemäße Planung sicherzustellen, sind die in Anhang 1 „Termine für die Haushaltsplanung“ genannten Termine zwingend einzuhalten.

2. Der Haushaltsplanentwurf des Vereins für den Geschäftsstellenbetrieb wird mit dem Vorstand und dem Finanzausschuss unter Einbeziehung des Vereinsrates beraten.  TuSLi-Finanzordnung | Version: 1.3.0 | 23.01.2018 Seite 2 von 6

3. Die Haushaltsplanentwürfe der Abteilungsleitungen werden mit dem für die Finanzen zuständigen Vorstandsmitglied und dem Finanzausschuss in einer gemeinsamen Sitzung beraten.

4. Die Haushaltsplanentwürfe der Abteilungsleitungen sind bis zum 15.09. für das folgende Geschäftsjahr per E-Mail beim Vorstand (vorstand@tusli.de) und beim Finanzausschuss
(finanzen@tusli.de) einzureichen.

5. Die satzungsgemäßen Beschlüsse über alle Abteilungshaushaltspläne sollen zur Planungssicherheit des Vereins und der Abteilungen und zur Wahrung der satzungsmäßigen Rechte der Mitglieder bis 31.10. des laufenden Geschäftsjahres erfolgt sein. Der Beschluss des Vereinshaushaltsplans erfolgt im Rahmen des Vereinstages.

6. Die Abteilungssonderbeiträge (ASB) decken die übungsleiterbezogenen und weiteren Abteilungskosten. Dabei hat jeweils eine aufwandsgerechte Planung, getrennt nach Sportbetrieb (ASB 1) und für die Bezahlung der Trainer / Übungsleiter (ASB 2), zu erfolgen.

7. Sofern sich die Finanzmittel einer Abteilung als nicht ausreichend erweisen, werden alle notwendigen Maßnahmen aufgrund eines Vereinsratsbeschlusses ergriffen. Hierzu zählt ausdrücklich auch ein Ersatzbeschluss zur Höhe der entsprechenden Abteilungssonderbeiträge (ASB 1 und ASB 2).

## § 3 Jahresabschluss

1. Der Jahresabschluss wird gemäß dem jeweils gültigen Einkommensteuergesetz erstellt.

2. Der gewählte Finanzausschuss prüft die Beauftragung der Erstellung des Jahresabschlusses an das Steuerbüro. Darüber hinaus kontrolliert er das Vorliegen des testierten Jahresabschlusses.

3. Der Vereinsrat und der Vorstand sorgen für die Einhaltung der Finanzordnung.

4. Stellt sich zum Ende eines Geschäftsjahrs heraus, dass Restmittel in der Abteilungskasse verbleiben, werden diese als Rückstellung der Abteilung gebucht und können zu einem späteren Zeitpunkt zu einer Verringerung des ASB führen. Stellt sich zum Ende eines Geschäftsjahres heraus, dass nicht mehr ausreichende Mittel zur Bezahlung der abteilungsgebundenen Kosten zur Verfügung stehen, wird zunächst der sportbezogene bzw.  übungsleiterbezogene ASB zur Zahlbarmachung der fälligen Beträge herangezogen. Reicht dieser Betrag nicht aus, muss ein internes Darlehen der Solidargemeinschaft zulasten künftiger Haushaltsjahre der betroffenen Abteilung aufgenommen werden, über das der Vereinsrat entscheidet. Maßnahmen gemäß §2 Absatz 7 bleiben hiervon unberührt.

## § 4 Verwaltung der Finanzmittel

1. Jede Abteilung besitzt ein Kontokorrent, auf dem die ASB-Anteile für den Sportbetrieb ohne ÜL-/AE-Kosten bereitgestellt werden, im Folgenden „Sportkasse“ genannt. Alle Einnahmen und Ausgaben der Sportkasse werden durch die Abteilungen gebucht und abgewickelt.

2. Jede Abteilung besitzt darüber hinaus ein virtuelles Buchungskonto, auf dem die ÜL-/AEKosten gebucht werden. Die Kontenstände werden den Abteilungsleitungen auf Anfrage mitgeteilt.

3. Der Verein stellt den Abteilungen in regelmäßigen Raten den Betrag für die Sportkasse zur Verfügung. Näheres regelt der Anhang 2 der Finanzordnung „Auszahlungsmodus Abteilungssonderbeiträge (ASB)“.

4. Im Rahmen ihres ASB-Jahresbudgets kann eine Abteilung Verschiebungen zwischen dem virtuellen ÜL-/AE-Konto und der Sportkasse mit dem Vorstand vereinbaren.

5. Sonderkonten bzw. Sonderkassen können vom Vorstand auf Antrag für Ausnahmefälle und zeitlich befristet genehmigt werden.

## § 5 Erhebung und Verwendung der Finanzmittel

1. Alle Mitgliedsbeiträge des Vereins werden vom Verein erhoben und gebucht. Von den Mitgliedsbeiträgen behält der Verein den Grundbeitrag pro Mitglied zur Deckung des Finanzbedarfs des Vereins ein.

2. Die jeweiligen Abteilungssonderbeiträge sind im Mitgliedsbeitrag enthalten. Nach deren Eingang erhalten die Abteilungen den sportbezogenen Anteil.

3. Für Nicht-Mitglieder besteht die Möglichkeit, zeitlich begrenzt abteilungsspezifische Leistungen mittels Kurskarten in Anspruch zu nehmen. Näheres regelt die Kurskartendurchführungsrichtlinie.

## § 6 Zahlungsverkehr

1. Der gesamte Zahlungsverkehr wird über die jeweils betroffene Kasse grundsätzlich bargeldlos abgewickelt, eine Ausnahme bilden die Kurskarten. Insbesondere dürfen alle ÜL- /AE-Ausgaben der Abteilungen nur über die Geschäftsstelle ausgezahlt werden.

2. Über jede Einnahme und Ausgabe muss ein Beleg vorhanden sein. Der Beleg muss den Tag der Ausgabe, den zu zahlenden Betrag, den Verwendungszweck und ggf. die Mehrwertsteuer bzw. die Umsatzsteuer enthalten.

3. Bei Sammelabrechnungen muss auf dem Deckblatt die Zahl der Einzelbelege vermerkt werden.

4. Vor der Anweisung eines übungsleiterbezogenen Rechnungsbetrags durch den Vorstand muss der Abteilungsleiter oder bei dessen Verhinderung dessen Vertreter die sachliche Berechtigung der Ausgabe durch seine Unterschrift bestätigen.

5. Wegen des Jahresabschlusses sind Barauslagen zum 30. Dezember des auslaufenden Jahres beim Vorstand abzurechnen, sofern dieser keine Ausnahme zulässt. Ebenso ist der interne Geldtransfer des Vereins so vorzunehmen, dass er bis zum 27. Dezember des auslaufenden Jahres abgeschlossen werden kann.

6. Alle Kassenbücher (sämtliche Finanzunterlagen einschließlich aller Belege und Kontoauszüge) dürfen in den jeweiligen Abteilungen maximal für das laufende und die zwei vorherigen Geschäftsjahre aufbewahrt werden. Kassenbücher zu länger zurück liegenden Geschäftsjahren sind bis Ende Februar an die Geschäftsstelle zur Archivierung zu übergeben. Nach Ablauf der gesetzlichen Aufbewahrungsfrist sorgt der Vorstand für eine datenschutzgerechte Entsorgung aller Kassenbücher.

## § 7 Eingehen von Verbindlichkeiten

1. Das Eingehen von Rechtsverbindlichkeiten ist im Rahmen eines vom Vereinstag verabschiedeten Haushaltsplanes für das Geschäftsjahr dem Vorstand uneingeschränkt gestattet. Bei Überschreiten von Etatpositionen um mehr als € 1.000, mindestens aber um 5%, ist die Zustimmung des Finanzausschusses einzuholen. Ab einem Betrag von € 5.000 muss zusätzlich der Vereinsrat zustimmen. Bei neuen Etatpositionen gelten die gleichen absoluten Grenzen. Übersteigen die Ausgaben abzüglich zusätzlicher Einnahmen die Summe aller Planansätze um € 25.000 oder mehr, muss ein Vereinstag einen Nachtragshaushalt beschließen. Für Großveranstaltungen gelten Sonderregelungen (siehe § 7, Ziffer 3).

2. Eine Abteilungsleitung darf im Rahmen des verabschiedeten Abteilungshaushaltsplans Verbindlichkeiten für den Büro- und Verwaltungsbedarf sowie für den laufenden Sportbetrieb bis zu einem Betrag von € 2.000 selbstständig eingehen.  TuSLi-Finanzordnung | Version: 1.3.0 | 23.01.2018 Seite 4 von 6 Die Zustimmung des Vorstands gilt bis zu diesen Beträgen als erteilt.

   Eine Abteilungsleitung darf im Rahmen des verabschiedeten Abteilungshaushaltsplans Verbindlichkeiten für Anschaffungen bis unter € 500 selbstständig eingehen. Die Zustimmung des Vorstands gilt bis zu diesem Betrag als erteilt.  Jedoch sollten alle Ausgaben ab € 500 dem Vorstand angezeigt werden, damit eventuelle Zuschüsse beantragt werden können. Gewährte Zuschüsse erhält die entsprechende Abteilung.

3. Eine Großveranstaltung ist eine Veranstaltung des Vorstandes, einer Abteilung oder mehrerer Abteilungen, bei der voraussichtlich mehr als € 2.000 ausgegeben werden. Sie muss mit entsprechenden Unterlagen kalkuliert und dem Finanzausschuss sowie dem Vorstand zur Genehmigung vorgelegt werden. Ab € 5.000 muss zusätzlich der Vereinsrat zustimmen. Ab € 25.000 muss außerdem ein Vereinstag zustimmen.

4. Es ist unzulässig, einen einheitlichen wirtschaftlichen Vorgang zu teilen, um dadurch die Zuständigkeit für die Genehmigung der Ausgabe zu begründen.

5. Über die abgeschlossene Großveranstaltung ist eine entsprechende Abrechnung zu erstellen und dem Finanzausschuss sowie dem Vorstand vorzulegen.

## § 8 Spenden

1. Nur der Verein ist berechtigt, steuerbegünstigte Zuwendungsbescheinigungen auszustellen.

2. Spenden müssen mit der Angabe der Zweckbestimmung an die Vereinskasse überwiesen werden. Der Vorstand stellt eine Zuwendungsbescheinigung aus und leitet diese an die Spender weiter. Die Spendenbeträge werden zeitnah an die begünstigte Abteilung weitergeleitet.

3. Spenden kommen dem Verein zugute, wenn sie vom Spender nicht ausdrücklich einer bestimmten Abteilung zugewiesen werden.

## § 9 Zuwendungen und Zuschüsse

1. Zuwendungen fließen dem Verein zu und sind ihrem Zweck entsprechend zu verwenden.  2. Zweckgebundene Zuwendungen für Abteilungen werden zeitnah an diese weitergeleitet.

3. Übungsleiterzuschüsse werden über die Summe aller von der Geschäftsstelle bezahlten und dem Landessportbund gegenüber abgerechneten Trainerstunden der Abteilungen des vergangenen Jahres prozentual auf die Abteilungen verteilt. Die Zuschüsse werden zeitnah nach dem Erhalt der letzten Rate abgerechnet und den virtuellen Buchungskonten der Abteilungen zugewiesen.

4. Der Vereinsrat berät zeitnah über die Verteilung von nicht zweck- oder abteilungsgebundenen Zuwendungen, zum Beispiel für die Jugend.

## § 10 Inkrafttreten

1. Die Finanzordnung wurde am 17.09.2009 erstellt und in der vorliegenden Fassung vom Vereinsrat am 23.01.2018 beschlossen. Die aktuelle Fassung gilt ab dem 24.01.2018.

Änderungen der §6 1, §9 5 am 21.09.2011

Änderungen der §2 3-7, §6 6, §7 1-4 am 16.09.2013

Änderungen der §2 1 u. 5, §4 2-5, §9 5 am 14.05.2014

Änderungen der Präambel, §2 1-5, §3 4, §5 3, §6 1,2,6, §7 1-3, §9 1-5 am 04.03.2015

Änderungen §3 1-3, §10 am 15.03.2016

Änderungen §1, §2, §3, §7 am 23.01.2018
