# Satzung

**Beschlossen: 16.11.2017**
Eintrag ins Vereinsregister: 08.03.2018

## Präambel

Bei personenbezogenen Bezeichnungen wird zur Vereinfachung in der Satzung und den Vereinsordnungen nur die männliche Form verwendet. Diese Bezeichnungen gelten jeweils in der geschlechtsspezifischen Form.

## § 1 NAME, SITZ DES VEREINS, GESCHÄFTS-JAHR UND VERBANDSZUGEHÖRIGKEIT

 1. Der am 28. April 1887 gegründete Verein hat den Namen Turn- und Sportverein Lichterfelde von 1887 (Berlin) e.V. (Kurzform: tuslichterfeldeberlin oder tusliberlin).
 2. Sitz des Vereins und Erfüllungsort ist Berlin.
 3. Der Verein ist im Vereinsregister des Amtsgerichts Berlin-Charlottenburg unter VR 4288 B eingetragen.
 4. Geschäftsjahr des Vereins ist das Kalenderjahr.
 5. Der Verein soll allen Fachverbänden angehören, deren Sportarten er betreibt. Er erkennt dessen Satzungen und Ordnungen an.

## § 2 ZWECK DES VEREINS

 1. Zweck des Vereins ist die Pflege von Leibesübungen zur Erhaltung und Steigerung der körperlichen Leistungsfähigkeit durch die Ausübung und Förderung des Wettkampf-, Leistungs-, Breiten-, Gesundheits- und Behindertensports sowie des Fitness- und Freizeitsports zur körperlichen Ertüchtigung seiner Mitglieder aller Altersgruppen.
 2. Darüber hinaus können Sportunterricht und -kurse für Nichtmitglieder mit der Option einer späteren Mitgliedschaft angeboten werden.
 3. In diesem Sinne sind die Betreuung und der Schutz der Kinder und Jugendlichen eine besondere Aufgabe.
 4. Parteipolitische, konfessionelle, rassische und berufssportliche Bestrebungen sind ausgeschlossen. Der Verein ist offen für alle Angehörigen aller Nationalitäten und Bevölkerungsgruppen.
 5. Die Bildung von und der Beitritt zu Gemeinschaften oder Kooperationen mit anderen Sportvereinen oder Einrichtungen im Rahmen des Vereinszwecks sind zulässig.
 6. Die Bildung von rechtlich selbstständigen Zweigvereinen im Rahmen des Vereinszwecks ist zulässig. Deren Satzung muss mit den Grundsätzen dieser Satzung vereinbar sein.

## § 3 GEMEINNÜTZIGKEIT UND FÖRDERUNGSWÜRDIGKEIT

 1. Der Verein verfolgt durch die Ausübung des Sports ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der Abgabenordnung, und zwar insbesondere durch die Pflege und Förderung der Leibesübungen nach den Grundsätzen des Amateursports.
 2. Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
 3. Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden.
 4. Den ehrenamtlich und unentgeltlich tätigen Organen und ihren Mitgliedern, sowie einzelnen Mitgliedern mit Vereins- oder Abteilungsaufgaben können Auslagen und Aufwendungen auf Nachweis erstattet werden. Die Zahlung einer angemessenen pauschalen Aufwandsentschädigung und/oder einer angemessenen pauschalen Auslagenerstattung ist zulässig im Rahmen von §3 Nr. 26 a Satz 1 EStG. Zahlungen dürfen nur auf Grund einer Regelung des jeweiligen Haushaltsplanes geleistet werden. Die Mitglieder des Vorstandes sind in dieser Funktion unentgeltlich tätig (§27 Abs. 3 BGB).
 5. Die Mitglieder erhalten keine Anteile aus Einnahmen und in ihrer Eigenschaft als Mitglieder auch keine sonstigen Zuwendungen aus Vereinsmitteln.
 6. Sie erhalten bei ihrem Ausscheiden oder bei Auflösung des Vereins nicht mehr als ihre etwa eingezahlten Kapitalanteile und/oder den gemeinen Wert ihrer geleisteten Sacheinlagen zurück.
 7. Niemand darf durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.
 8. Die Förderungswürdigkeit hat das Bezirksamt Steglitz von Berlin am 8. Mai 1947 festgestellt.
 9. Der Verein ist förderungswürdig nach § 3 des Sportförderungsgesetzes Berlin.
 10. Er gilt als anerkannter Träger der freien Jugendhilfe nach § 75 SGB VIII und § 40 AG KJHG

## § 4 VEREINSZEICHEN UND -FARBEN

 1. Das Vereinszeichen ist ein schwarzes L im schwarzen Kreis auf weißem Grund.
 2. Die Vereinsfarben sind Schwarz und Weiß.

## § 5 BEKANNTMACHUNGEN

 1. Bekanntmachungen für Mitglieder werden unter www.tusli.de im Internet veröffentlicht.
 2. Mitteilungen an alle Mitglieder erfolgen schriftlich, entweder per E-Mail, bei Mitgliedern, die dem schriftlich zugestimmt haben, oder per Post bei allen anderen

## § 6 HAFTUNG DES VEREINS

 1. Die Organhaftung des Vereins und seiner Organmitglieder (gegenüber Dritten) richtet sich nach §31 BGB.
 2. Die Haftung der unentgeltlich tätigen Organmitglieder und besonderen Vertretern (§15 Sätze 1 und 3 der Satzung) gegenüber dem Verein richtet sich nach §31a Abs. 1 BGB.
 3. Die Haftung von anderen unentgeltlich für den Verein tätigen Vereinsmitgliedern richtet sich nach §31b Abs. 1 BGB.
 4. Sind Organmitglieder, besondere Vertreter und Vereinsmitglieder nach den Sätzen 2 und 3 einem anderen zum Ersatz eines Schadens verpflichtet, können sie unter den Voraussetzungen von §31a Abs. 2 und §31b Abs. 2 BGB vom Verein die Befreiung von der Verbindlichkeit verlangen.
 5. Die Mitglieder des Vereins sind im Rahmen der Sportversicherung des Landessportbundes Berlin e.V. bei der unmittelbaren sportlichen Betätigung subsidiär unfallversichert und bei der Betätigung im Interesse des Vereins bei Veranstaltungen nachrangig haftpflichtversichert.
 6. Der Verein behält sich vor, zusätzliche Versicherungen zum Schutze seiner Mitglieder abzuschließen.
 7. Der Verein schließt mit dem Aufnahmevertrag die Haftung für Schäden aus, die über die in den Versicherungsverträgen vorgesehenen Leistungen hinausgehen.
 8. Die Haftung des Vereins für Verbindlichkeiten der Zweigvereine (§2 Satz 6) ist ausgeschlossen.

## § 7 AUFLÖSUNG DES VEREINS

 1. Über die Auflösung des Vereins kann nur ein zu diesem Zweck einberufener Vereinstag mit einer Dreiviertelmehrheit der abgegebenen Stimmen der stimmberechtigten Mitglieder entscheiden, sofern mindestens ein Drittel der stimmberechtigten Vereinsmitglieder anwesend ist.
 2. Wird eine vom Vorstand oder vom Vereinsrat beantragte Auflösung abgelehnt oder kommt sie wegen Beschlussunfähigkeit des Vereinstages nicht zustande, kann ein weiterer zu diesem Zweck einberufener Vereinstag mit Dreiviertelmehrheit der abgegebenen Stimmen der stimmberechtigten Mitglieder die Auflösung des Vereins beschließen.
 3. Der Vereinstag beschließt über die Bestellung von mindestens zwei Liquidatoren.
 4. Zweigvereine nach §2 Satz 6 betrifft die Auflösung nicht.
 5. Bei Auflösung des Vereins oder bei Wegfall steuerbegünstigter Zwecke fällt sein etwaige Verbindlichkeiten übersteigendes Vermögen an den Landessportbund Berlin e.V., der es unmittelbar und ausschließlich für steuerbegünstigte Zwecke zu verwenden hat.

## § 8 ERWERB DER MITGLIEDSCHAFT

 1. Mitglied kann jede natürliche Person werden.
 2. Die Mitglieder der Zweigvereine sind gleichzeitig Mitglieder des Vereins. Abweichende Rechte und Pflichten der Mitglieder der Zweigvereine regelt vorrangig deren Satzung für ihre Sportart/Sportarten und die Vereinbarung zwischen dem Vorstand des Vereins und dem Vorstand des jeweiligen Zweigvereins. Sätze 3-9 sind für sie nicht anzuwenden.
 3. Die Aufnahme ist schriftlich unter vollständiger Ausfertigung des Aufnahmeantrages und Benennung der gewünschten Sportart oder Abteilung zu beantragen. Die jeweils gültigen Datenschutzbestimmungen hat der Verein zu beachten.
 4. Bei Aufnahmeanträgen Minderjähriger ist die schriftliche Zustimmung eines gesetzlichen Vertreters erforderlich.
 5. In dem Aufnahmeantrag übernimmt der gesetzliche Vertreter die persönliche Haftung für die Beitragsschuld des Minderjährigen.
 6. Mit dem Antrag erkennt der Bewerber bzw. sein gesetzlicher Vertreter für den Fall der Aufnahme die Satzung an.
 7. Der Vorstand entscheidet über die Aufnahme und bestätigt sie schriftlich.
 8. Die Ablehnung einer Aufnahme braucht nicht begründet zu werden.
 9. Zurückgewiesene Antragsteller können binnen eines Monats nach der Ablehnung schriftlich bei der Vereinsgeschäftsstelle Einspruch erheben, über den der Vereinsrat endgültig entscheidet.
 10. Die Mitgliedschaft und die Ausübung von Stimm- und anderen Mitgliedschaftsrechten sind nicht übertragbar und nicht vererblich.
 11. Die Mitgliedschaft kann aktiv oder passiv ausgeübt werden.
 12. Die Mitglieder von Zweigvereinen sind korporative Mitglieder des Vereins.
 13. Sonderregelungen für einzelne Mitgliedergruppen insbesondere über Zeitdauer, Beitragspflicht und Kündigungsrecht sind mit Beschluss des Vorstandes möglich.

## § 9 RECHTE DER MITGLIEDER

 1. Jedes aktive Mitglied hat das Recht, an allen Sportarten teilzunehmen, die im Verein betrieben werden, sofern es der Übungs- und Wettkampfbetrieb erlaubt und die damit verbundenen Verpflichtungen übernommen werden. Mitglieder nach §8 Satz 13 können nur an der von ihnen gewählten Sport- oder Kursart teilnehmen.
 2. Jedes Mitglied hat das Recht, den Schlichtungsausschuss anzurufen.
 3. Volljährige, unbeschränkt geschäftsfähige Mitglieder und Mitglieder, die das 16. Lebensjahr vollendet haben, besitzen volles Stimmrecht. Volljährige, unbeschränkt geschäftsfähigeMitglieder mit Ausnahme der Mitglieder nach §8 Satz 13 besitzen das passive Wahlrecht für alle Vereinsämter. §17 Satz 3 und §18 Satz 4 bleiben unberührt.
 4. Mitglieder vom vollendeten 14. bis zum vollendeten 27. Lebensjahr besitzen für die Wahl von Jugendwarten und Jugendvertretern Stimmrecht.
 5. Mitglieder der Zweigvereine sowie Mitglieder des Vereins bis zum vollendeten 14. Lebensjahr besitzen kein Stimmrecht.
 6. Die Ausübung von Vereinsämtern muss persönlich erfolgen.

## § 10 PFLICHTEN DER MITGLIEDER

 1. Alle Mitglieder sind verpflichtet, die satzungsgemäßen Zwecke des Vereins nach besten Kräften zu unterstützen, sich entsprechend der Satzung, den weiteren Ordnungen des Vereins sowie der Beschlüsse der Vereins- und Abteilungsorgane zu verhalten und die Weisungen der Verantwortlichen zu befolgen.
 2. Die Mitglieder sind zur Zahlung der Aufnahmegebühr, der Beiträge und eventueller Sonderumlagen verpflichtet. Der Jahresbeitrag setzt sich zusammen aus dem Jahresgrundbeitrag und den Abteilungsbeiträgen und -umlagen der genutzten Abteilungen.  Über die Höhe der Jahresgrundbeiträge und ggf. der Vereinsumlagen entscheidet der Vereinstag. §8 Satz 13 bleibt unberührt. Alle Beiträge und Umlagen werden vom Verein erhoben. \*Zweigvereine entrichten korporativ für die Mitglieder ihrer Sportart einen vereinbarten Beitrag je Mitglied an den Verein.
 3. Der Jahresbeitrag ist am 1. Januar fällig und zum 20. Januar zahlbar.
 4. Im Falle des Verzugs erhöht sich der Beitrag im 1. Kalenderhalbjahr und danach in jedem folgenden Kalenderhalbjahr. \*Die Erhöhungsbeiträge regelt die Beitragsordnung.
 5. Weiteres regelt die Beitragsordnung.
 6. Beitragssäumige Mitglieder kann der Vorstand für die Zeit des Verzugs von ihren satzungsmäßigen Rechten ausschließen.
 7. Der Vorstand ist in Ausnahmefällen berechtigt, einzelnen Mitgliedern Beiträge zu stunden, zu ermäßigen oder zu erlassen.
 8. Ehrenmitglieder und Mitglieder, die dem Verein mindestens 50 Jahre ununterbrochen angehören, sind beitragsfrei. Bestehende Beitragsfreiheiten bleiben bestehen.

## § 11 ERLÖSCHEN DER MITGLIEDSCHAFT

 1. Die Mitgliedschaft endet durch:
    - Tod,
    - Austritt,
    - Ausschluss,
    - Zeitablauf,
    - Streichung oder
    - Löschung des Vereins

## § 12 AUSTRITT

 1. Der Austritt ist nur zum Ende eines Kalenderjahres möglich. §8 Satz 13 bleibt unberührt.
 2. Die schriftliche Austrittserklärung muss bis zum 30. November in der Geschäftsstelle vorliegen.
 3. Austrittserklärungen Minderjähriger, bedürfen der schriftlichen Zustimmung des gesetzlichen Vertreters.
 4. Ein Austritt ist rückwirkend nicht möglich.
 5. Die verbandsrechtliche Freigabe ist vom Zeitpunkt des Eingangs der Austrittserklärung an möglich, sofern das Mitglied seine materiellen Pflichten gegenüber dem Verein erfüllt hat.
 6. Nach Beendigung der Mitgliedschaft bleibt die Zahlungspflicht der bis zu diesem Zeitpunkt fällig gewordenen Beiträge bestehen.
 7. Ausgeschiedene, gestrichene oder ausgeschlossene Mitglieder haben keinen Anspruch auf Anteile aus dem Vermögen des Vereins. Andere Ansprüche derartiger Mitglieder müssen binnen 3 Monaten nach dem Ende der Mitgliedschaft durch eingeschriebenen Brief an die
Geschäftsstelle schriftlich dargelegt und geltend gemacht werden. Vereinseigentum ist
unverzüglich dem Verein auszuhändigen.

## § 13 MASSREGELUNG
 1. Gegen Mitglieder können vom Vorstand Maßregelungen beschlossen werden:
    - wegen erheblicher Verletzungen satzungsgemäßer Verpflichtungen bzw. Verstoßes gegen
    - Ordnungen oder Beschlüsse sowie Anordnungen der Vereinsorgane und Abteilungsorgane,
    - wegen vereinsschädigenden Verhaltens, eines schweren Verstoßes gegen die Interessen des Vereins oder groben unsportlichen Verhaltens
    - wegen unehrenhafter Handlungen.

    Mitglieder können aus dem Verein ausgeschlossen werden, wenn sie gegen:
    - den Zweck des Vereins,
    - die Satzung oder Vereinsordnungen,
    - Beschlüsse oder Anordnungen der Vereins- oder Abteilungsorgane verstoßen,
    - das Ansehen des Vereins geschädigt oder sich unehrenhafter Handlungen schuldig gemacht haben.
 2. Maßregelungen sind:
    - Verweis
    - Befristetes Verbot der Teilnahme am Sportbetrieb sowie an Veranstaltungen des Vereins,
    - Ausschluss aus dem Verein.
    Sie werden mit Bekanntgabe wirksam.
 3. Beitragssäumige Mitglieder können nach erfolglosem (außergerichtlichem) Mahnverfahren bzw.  nicht Erreichens im Einverständnis zwischen Vorstand und betroffener Abteilung aus dem Verein ausgeschlossen bzw. gestrichen werden.
 4. Bis dahin entstandene materielle Mitgliedspflichten sind zu erfüllen.
 5. Dem Mitglied steht das Recht des Einspruchs innerhalb eines Monats nach Bekanntgabe der Maßregelung oder der Streichung beim Schlichtungsausschuss zu. Das Mitglied ist anzuhören.  Der Einspruch hat aufschiebende Wirkung.
 6. Gegen dessen Entscheid kann binnen eines Monats der Beschluss der nächsten Sitzung des Vereinsrates schriftlich in der Geschäftsstelle beantragt werden.
 7. Das Recht auf gerichtliche Nachprüfung der Maßregelung bleibt unberührt.

## § 14 EHRUNGEN

 1. Mitglieder können für
    - besondere Verdienste um den Verein und den Sport und
    - langjährige Treue geehrt oder mit der Ehrenmitgliedschaft ausgezeichnet werden.
 2. Näheres bestimmt die Ehrenordnung.

## § 15 ORGANE DES VEREINS

 1. Organe des Vereins sind · der Vereinstag, · der Vorstand, · der Vereinsrat. Sie sind nach Maßgabe der nachfolgenden Regelungen für die Außenvertretung und für die den gesamten Verein betreffenden Belange zuständig.
 2. Über die Versammlungen der Vereinsorgane sind Protokolle zu führen, die vom Versammlungsleiter und vom Protokollführer zu unterzeichnen sind.
 3. Organe der Abteilungen sind die Abteilungsversammlung und die Abteilungsleitung für ihren Aufgabenbereich.

## § 16 VEREINSTAG

 1. Der Vereinstag, der mindestens einmal jährlich einberufen werden muss, ist oberstes Vereinsorgan und die Versammlung der Vereinsmitglieder nach dem vollendeten 14. Lebensjahr.
    Er findet im November statt.
 2. Er muss außerdem zusammentreten, wenn der Vorstand, der Vereinsrat oder ein Zehntel der Mitglieder es schriftlich unter Angabe des Zwecks und der Gründe verlangen. Für dieses Verlangen ist ein gesetzlicher Vertreter der für den Vereinstag nach §9 der Satzung nicht stimmberechtigten Mitglieder antrags- und für den Zweck des Verlangens stimmberechtigt.
 3. Er wird vom Vorstand wenigstens zwei Wochen vorher unter Bekanntgabe der Tagesordnung schriftlich einberufen. Mitglieder, die dem E-Mail-Versand schriftlich zugestimmt haben, werden per E-Mail eingeladen.
 4. Anträge, die auf dem Vereinstag behandelt werden sollen, müssen spätestens bis zum 30.  September in maschinengeschriebener Form in der Geschäftsstelle eingegangen sein.
 5. Später eingegangene sowie während des Vereinstages gestellte Anträge für die Tagesordnung können nur auf Beschluss des Vereinstages behandelt werden, wenn ihre Dringlichkeit mit einer Zweidrittelmehrheit bejaht wird.
 6. Anträge auf Satzungsänderung müssen spätestens mit der Bekanntgabe der Tagesordnung wörtlich mitgeteilt werden; Dringlichkeitsanträge sind ausgeschlossen.
 7. Unterlagen, die auf dem Vereinstag behandelt werden sollen, sind mindestens eine Woche vorher den Mitgliedern zugänglich zu machen.
 8. Jeder ordnungsgemäß einberufene Vereinstag ist ohne Rücksicht auf die Zahl der erschienenen stimmberechtigten Mitglieder beschlussfähig, wenn die Satzung nichts anderes bestimmt.
 9. Das Stimmrecht richtet sich nach § 9.
 10. Der Vereinstag beschließt über
    - Satzungsänderungen,
    - die Entlastung des Vorstandes und des Vereinsrates auf Antrag des Finanzausschusses,
    - die Zahl der Vorstandsmitglieder,
    - die Aufnahmegebühr, den Jahresgrundbeitrag und Sonderumlagen,
    - den Haushaltsplan der in Einnahmen und Ausgaben ausgeglichen ist,
    - Anträge,
    - dingliche Geschäfte und Kreditaufnahmen, sofern es die Finanzordnung erfordert,
    - die Auflösung des Vereins (§ 7).
 11. Der Vereinstag wählt zu Beginn der Sitzung einen Schriftführer.
 12. Der Vereinstag wird von einem Vorstandsmitglied oder einem auf Vorschlag des Vorstandes vom Vereinstag bestimmten Mitglied geleitet.
 13. Der Vereinstag wählt bis zum übernächsten ordentlichen Vereinstag
    - den Vorstand,
    - Vereinswarte (nach Bedarf) und
    - die Vereinsausschüsse.
 14. Die Wahlen werden durch die Wahlordnung geregelt. Sie sind grundsätzlich in Einzelwahl durchzuführen. Blockwahl ist durch Beschluss möglich. Auf Antrag eines stimmberechtigten Mitgliedes muss eine geheime Wahl vorgenommen werden.
 15. Dem Vereinstag sind die Jahresberichte des Vorstandes, des Vereinsrats, des Finanz- und des Schlichtungsausschusses und gegebenenfalls der Vereinswarte vorzutragen.
 16. Beschlüsse werden mit einfacher Mehrheit der abgegebenen gültigen Stimmen gefasst.  Stimmenthaltungen und ggf. ungültige Stimmen werden nicht gezählt. Stimmengleichheit bedeutet Ablehnung.
 17. Für Satzungsänderungen ist eine Dreiviertelmehrheit der abgegebenen gültigen Stimmen erforderlich.
 18. Der Versammlungsleiter kann die Öffentlichkeit ausschließen.

## § 17 VEREINSAUSSCHÜSSE UND VEREINSWARTE

 1. Vereinsausschüsse sind
    - der Finanzausschuss,
    - der Schlichtungsausschuss,
    - der Wahlausschuss.
 2. Die Vereinsausschüsse bestehen aus mindestens drei Mitgliedern.
 3. Die Mitglieder des Schlichtungsausschusses dürfen nicht dem Vorstand, dem Vereinsrat, einer Abteilungsleitung (§ 23 Satz 1) oder dem Finanzausschuss angehören. Die Mitglieder des Finanzausschusses dürfen nicht dem Vorstand oder dem Schlichtungsausschuss angehören.
 4. Der Finanzausschuss prüft die Finanzen und die Einhaltung der Wirtschaftlichkeit des Vereins.  Näheres regeln die Finanzordnung und die Geschäftsordnung des Finanzausschusses.
 5. Der Schlichtungsausschuss hat die Aufgabe, über Einsprüche gegen Maßregelungen und Beschwerden aller Art zu befinden und Unstimmigkeiten zu schlichten.
Für Beschwerden und Unstimmigkeiten gilt § 13 Satz 6 sinngemäß. Die Betroffenen sind anzuhören.
 6. Der Wahlausschuss macht Vorschläge für die Anzahl und die Wahl der Mitglieder des Vorstands und jedes durch Wahl auf dem Vereinstag zu besetzende Amt.
    Seine weiteren Aufgaben bestimmt die Wahlordnung.
 7. Vereinswarte können für bestimmte Aufgaben des gesamten Vereins eingerichtet werden. Die Arbeit geschieht im Einvernehmen mit Vorstand und Vereinsrat. Mindestens 20 Vereinsmitglieder, der Vorstand oder der Vereinsrat können mit Aufgabenbeschreibung und Wahlvorschlag die Wahl von Vereinswarten nach §16 Satz 13 beantragen.

## § 18 DER VORSTAND

 1. Den Vorstand im Sinne des § 26 BGB bilden mindestens drei und höchstens sieben Mitglieder, von denen zwei gemeinsam berechtigt sind, rechtsverbindliche Handlungen vorzunehmen.
 2. Ferner ist das Vorstandsmitglied für Finanzen oder dessen Vertreter verpflichtet, bei bedeutsamen Ausgaben gemeinsam mit einem Abteilungsleiter in dessen Aufgabenbereich (Besonderer Vertreter nach § 30 BGB gemäß § 23 der Satzung) den Verein rechtsgeschäftlich zu vertreten. Näheres regelt die Finanzordnung.
 3. Der Vorstand führt die Geschäfte des Vereins im Sinne der Satzung und nach den Vereinsordnungen und -Richtlinien. Er ordnet und überwacht die Angelegenheiten des Vereins.
 4. Der Vorstand wird im Rahmen einer Geschäftsverteilung, die er sich selber gibt, tätig. Der Vorstand wählt binnen eines Monats einen Vorsitzenden, einen stellvertretenden Vorsitzenden sowie den Verantwortlichen für Finanzen. Aufgaben- und Ämterverteilung sind bekannt zu geben.
 5. Ein Vorstandsmitglied soll nicht gleichzeitig Leiter einer Abteilung sein.
 6. Der Vereinsjugendwart, der in einer Sitzung der Abteilungsjugendwarte gewählt wird, gehört dem Vorstand mit beratender Stimme an.
 7. Scheidet ein Mitglied des Vorstandes vorzeitig aus, so muss bei Unterschreiten der Mindestzahl nach Satz 1, im Übrigen kann, der Vereinsrat unverzüglich ein Vereinsmitglied als Nachfolger bis zum Ablauf der Amtszeit des Vorstandes bestellen oder die Einberufung des Vereinstags verlangen.
 8. Der Vorstand ist an die Beschlüsse des Vereinstages und des Vereinsrates gebunden.
 9. Er unterrichtet den Vereinsrat schriftlich über seine Beschlüsse und Vorhaben.
 10. Er kann zur Bewältigung von Vereinsaufgaben ehrenamtliche oder bezahlte Kräfte einsetzen.
 11. Der Vorstand oder eines seiner Mitglieder haftet gegenüber dem Verein nur für grob fahrlässiges oder vorsätzliches Handeln (§ 31a Abs. 1 BGB).
 12. Vorstandssitzungen können von jedem Mitglied des Vorstandes einberufen und geleitet werden.
 13. Der Vorstand ist beschlussfähig, wenn mehr als die Hälfte seiner Mitglieder anwesend ist.
 14. Der Vorstand entscheidet mit einfacher Mehrheit.
 15. Der Vorstand oder eines seiner Mitglieder kann zu allen Sitzungen aus besonderen Gründen Dritte hinzuziehen.
 16. Die Tätigkeit des Vorstandes wird durch die Geschäftsordnung des Vorstandes geregelt.
 17. Er ist für die Aufgaben nach § 2 Satz 2 der Satzung zuständig und entscheidet im Einvernehmen mit den Abteilungen über die Unterrichts- und Kursgebühren.

## § 19 DER VEREINSRAT

 1. Der Vereinsrat besteht aus
    - den Mitgliedern des Vorstandes,
    - den Vereinswarten und
    - maximal zwei Mitgliedern jeder Abteilungsleitung (Vertretungen möglich).
    Ehrenmitglieder und ein Mitglied des Vorstandes des jeweiligen Zweigvereins können an Vereinsratssitzungen mit beratender Stimme teilnehmen.
    Die Termine werden nach §5 Satz 1 bekannt gegeben.
 2. Der Vereinsrat beschließt über
    - Richtlinien für die Vereinsarbeit,
    - die Gründung und Auflösung von Abteilungen,
    - die Mitgliedschaft in Sportgemeinschaften und Kooperationen nach §2 Satz 5,
    - Zuweisungen nach Maßgabe der Finanzordnung und des Haushaltsplans an die Vereinsjugend, für die Vereinskinder und Abteilungen,
    - die Erhebung von Abteilungsbeiträgen und -umlagen, sofern ein Verstoß gegen die Finanzordnung vorliegt,
    - die Vereinskleidung,
    - Ehrungen (§ 14),
    - Aberkennung von Ehrungen, wenn sich der Geehrte eines sport- oder vereinsschädigenden Verhaltens schuldig gemacht hat,
    - die Maßregelung von Mitgliedern nach § 13 Satz 6,
    - die Einrichtung von Sportgruppen außerhalb von Abteilungen,
    sowie in ähnlichen, grundsätzlichen Angelegenheiten.
 3. Die nicht dem Vorstand angehörenden Vereinsratsmitglieder unterrichten den Vorstand und den Vereinsrat über alle wichtigen Angelegenheiten ihrer Arbeitsbereiche.
 4. Der Vereinsrat tagt nach Bedarf, wobei § 16 Sätze 3, 5 bis 8, 13, 15, 17 und 18 entsprechend gelten. Anträge sind spätestens 10 Tage vor einem Sitzungstermin dem Vorstand schriftlich einzureichen.
 5. Er muss einberufen werden, wenn zwei Vereinsratsmitglieder dies schriftlich unter Angabe der Gründe verlangen.
 6. Der Vereinsrat wählt aus seiner Mitte einen Sitzungsleiter und einen Protokollführer.

## § 20 BEIRÄTE
 1. Zur Behandlung spezieller Fachfragen und zur Durchführung besonderer Aufgaben kann der Vorstand Beiräte bestellen.
 2. Ihre erste Sitzung wird von einem Vorstandsmitglied einberufen.
 3. Die Bestellung endet spätestens mit der Amtszeit des Vorstandes.
 4. Der Vorstand kann von den Abteilungen die Benennung geeigneter Mitglieder verlangen.

## § 21 DIE ABTEILUNGEN DES VEREINS

 1. Der Verein ist in rechtlich unselbstständige Abteilungen gegliedert.
 2. Den Abteilungen obliegen für ihre Abteilungszugehörigen die sportlichen Aufgaben ihrer Fachbereiche sowie die Vertretung bei ihren Fachverbänden, wobei dem Vorstand auf dessen Verlangen eine Stimme einzuräumen ist.
 3. Sie sind für die satzungsgemäße Verwaltung ihrer Mittel im Rahmen der Finanzordnung und des Haushaltsplanes der Abteilung verantwortlich.
 4. Die Vorstandsmitglieder sind zu allen Abteilungsversammlungen über die Geschäftsstelle einzuladen und können ohne Stimmrecht teilnehmen.
 5. Einen Abteilungswechsel im laufenden Kalenderjahr regelt die Beitragsordnung. §12 Sätze 2, 3 und 6 sind sinngemäß anzuwenden.

## § 22 DIE ABTEILUNGSVERSAMMLUNG

 1. Die Abteilungsversammlung ist die Zusammenkunft der Abteilungszugehörigen nach dem vollendeten 14. Lebensjahr. Gesetzliche Vertreter von Kindern können mit beratender Stimme teilnehmen. Es bedarf hierzu keiner besonderen Einladung.
 2. Sie ist jährlich mindestens einmal von der Abteilungsleitung einzuberufen und soll im Oktober stattfinden. Anträge sind bis 10 Tage vor der Versammlung schriftlich bei der Abteilungsleitung einzureichen.
 3. §5, § 15 Satz 2, § 16 Sätze 2, 3, 6 bis 9, 13 und 15 bis 18 gelten entsprechend. Näheres regeln die Geschäftsordnungen und die Wahlordnung.
 4. Das Stimmrecht und Wahlrecht richten sich nach § 9. § 10 Satz 6 gilt entsprechend.
 5. Sie wählt die Abteilungsleitung für mindestens ein Jahr, maximal zwei Jahre und entscheidet über deren Abwahl.
    Sie soll Abteilungskassenprüfer wählen und über deren Abwahl entscheiden.
 6. Sie beschließt über die Entlastung der Abteilungsleitung und ihrer Kassenprüfer, über den Haushalt der Abteilung (entsprechend §16 Satz 10), Abteilungsbeiträge und -umlagen sowie Kursgebühren (diese vorbehaltlich der Zustimmung des Vorstandes und des Vereinsrates), Anträge und alle wichtigen Abteilungsangelegenheiten.

## § 23 DIE ABTEILUNGSLEITUNG

 1. Die Abteilungsleitung besteht aus dem Abteilungsleiter und mindestens zwei weiteren Mitgliedern, von denen eines für Finanzen zuständig sein muss.
 2. Jede Abteilungsleitung ist für die Durchführung der Abteilungsaufgaben verantwortlich und an die Satzung, die Vereinsordnungen und die Beschlüsse der Vereinsorgane und der Abteilungsversammlung gebunden.
 3. Die Abteilungsleitung tagt nach Bedarf und fasst ihre Beschlüsse mit einfacher Mehrheit der anwesenden Mitglieder.
 4. Die Abteilungsleitung informiert den Vorstand über die Pläne und Beschlüsse und die Termine der Jahresversammlungen ihrer Fachverbände.
 5. Der Abteilungsleiter vertritt den Verein rechtsgeschäftlich nach § 30 BGB im Rahmen seines Aufgabenbereiches und des Haushaltsplanes seiner Abteilung gemeinsam mit einem Vorstandsmitglied in bedeutsamen Angelegenheiten (nach § 18 Satz 2), in übrigen Angelegenheiten gemeinsam mit einem weiteren Abteilungsleitungsmitglied als besonderer Vertreter.
 6. Die Abteilungsleitung haftet gegenüber dem Verein nur für grob fahrlässiges oder vorsätzliches Handeln. §6 Sätze 1 bis 4 gelten entsprechend.
 7. Kommt durch die Wahlen nach § 22 eine Abteilungsleitung nach § 23 Satz 1 nicht zustande oder scheidet ein Mitglied der Abteilungsleitung aus, ist unter Beteiligung von Abteilungszugehörigen der § 18 Satz 6 sinngemäß anzuwenden.

## § 24 VEREINSORDNUNGEN

 1. Die Wahlordnung und die Beitragsordnung und deren Änderungen werden vom Vereinstag beschlossen.
 2. Die Jugendordnung und deren Änderungen werden von Mitgliedern des Vereins nach § 9 Satz 4 beschlossen, bedürfen der Zustimmung des Vereinsrates und werden dem Vereinstag zur Kenntnis gegeben.
 3. Die Finanzordnung und die Ehrenordnung sowie deren Änderungen werden vom Vereinsrat beschlossen und dem Vereinstag zur Kenntnis gegeben. Die Geschäftsordnungen sowie deren Änderungen werden vom jeweiligen Organ beschlossen und dem Vereinsrat zur Kenntnis gegeben.
 4. Alle Vereinsordnungen stehen in der Geschäftsstelle zur Einsicht zur Verfügung.

## § 25 SCHLUSSBESTIMMUNGEN

 1. Beim Inkrafttreten der Satzung vorhandene Vereins- und Abteilungsorgane bleiben bis zum Ende ihrer jeweiligen Wahlperiode bestehen.
 2. Die Satzung des Turn- und Sportvereins Lichterfelde von 1887 (Berlin) e.V. wurde am 04. Mai 1947 errichtet und in der vorliegenden Fassung am 16.11.2017 beschlossen.
